package dao

import (
	"errors"
	"waylar-test/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type MoviesDAO struct {
	Server   string
	Database string
}

var gdb *gorm.DB

func (m *MoviesDAO) Connect() {
	var err error
	gdb, err = gorm.Open(m.Database, m.Server)
	gdb.LogMode(true)
	if err != nil {
		panic(err)

	}
	gdb.AutoMigrate(
		&models.Movie{},
	)

}

func (m *MoviesDAO) GetAll() ([]models.Movie, error) {

	movies := []models.Movie{}
	err := gdb.Find(&movies).Error
	if err != nil {
		return nil, err
	}
	return movies, nil
}

func (m *MoviesDAO) GetByID(id uint) (*models.Movie, error) {
	movie := models.Movie{}
	if gdb.Where("id = ?", id).First(&movie).RecordNotFound() {
		return nil, errors.New("Record not found")
	}
	return &movie, nil
}

func (m *MoviesDAO) Insert(movie models.Movie) error {
	err := gdb.Save(&movie).Error
	return err
}

func (m *MoviesDAO) Update(movie models.Movie) error {
	if gdb.Where("id = ?", movie.ID).First(&movie).RecordNotFound() {
		return errors.New("Record not found")
	}
	err := gdb.Save(&movie).Error
	return err
}

func (m *MoviesDAO) Delete(movie models.Movie) error {
	if gdb.Where("id = ?", movie.ID).First(&movie).RecordNotFound() {
		return errors.New("Record not found")
	}
	err := gdb.Delete(&movie).Error
	return err
}

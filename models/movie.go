package models

type Movie struct {
	ID          uint   `gorm:"primary_key" json:"id"`
	MovieName   string `json:"movieName"`
	Director    string `json:"director"`
	ReleaseYear string `json:"releaseYear"`
}

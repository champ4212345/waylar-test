package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strconv"
	"waylar-test/dao"
	"waylar-test/models"

	"github.com/gorilla/mux"
)

var db = dao.MoviesDAO{}

type ResponseMessage struct {
	Message string `json:"message"`
}

var config map[string]string

func init() {
	config = make(map[string]string)
	file, _ := os.Open("config.json")
	decoder := json.NewDecoder(file)
	err := decoder.Decode(&config)
	if err != nil {
		panic("can t read config")
	}

	db.Server = config["movie_mysql_url"]
	db.Database = config["movie_database_type"]
	db.Connect()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/movies", GetAllMovie).Methods("GET")
	r.HandleFunc("/movies", CreateMovie).Methods("POST")
	r.HandleFunc("/movies", UpdateMovie).Methods("PUT")
	r.HandleFunc("/movies", DeleteMovie).Methods("DELETE")
	r.HandleFunc("/movies/{id}", GetMovieByID).Methods("GET")
	if err := http.ListenAndServe(config["port"], r); err != nil {
		panic(err)
	}
}

func GetAllMovie(w http.ResponseWriter, r *http.Request) {
	movies, err := db.GetAll()
	if err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	respondWithJson(w, http.StatusOK, movies)
}

func GetMovieByID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.ParseUint(params["id"], 10, 32)
	if err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	movie, err := db.GetByID(uint(id))
	if err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	respondWithJson(w, http.StatusOK, movie)
}

func CreateMovie(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	movie := models.Movie{}
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	if movie.ID != 0 {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: errors.New("Bad request").Error()})
		return
	}
	if err := db.Insert(movie); err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	respondWithJson(w, http.StatusOK, ResponseMessage{Message: "success"})
}

func UpdateMovie(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	movie := models.Movie{}
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	if err := db.Update(movie); err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	respondWithJson(w, http.StatusOK, ResponseMessage{Message: "success"})
}

func DeleteMovie(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	movie := models.Movie{}
	if err := json.NewDecoder(r.Body).Decode(&movie); err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	if err := db.Delete(movie); err != nil {
		respondWithJson(w, http.StatusBadRequest, ResponseMessage{Message: err.Error()})
		return
	}
	respondWithJson(w, http.StatusOK, ResponseMessage{Message: "success"})
}

func respondWithJson(w http.ResponseWriter, code int, data interface{}) {
	response, _ := json.Marshal(data)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
